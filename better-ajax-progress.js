(function($) {
  // These functions are copy/pasted from core's ajax.js, proceed with caustion.
  // Please use comments to highlight what has been changed
  // Remove all ajax-throbbers from Drupal, and instead use a class on the element
  // that get's the ajax-call.
  Drupal.Ajax.prototype.setProgressIndicatorThrobber = function () {
    // Add an extra class to the element
    $(this.element).addClass('js-loading');

    // No longer do anything else
    /*
    this.progress.element = $('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div>');
    if (this.progress.message) {
      this.progress.element.find('.throbber').after('<div class="message">' + this.progress.message + '</div>');
    }
    $(this.element).after(this.progress.element);
    */
  };

  Drupal.Ajax.prototype.setProgressIndicatorFullscreen = function () {
    // fullscreen-throbber, I don't think, so, just do the same thing as a normal throbber.
    $(this.element).addClass('js-loading');

    // No longer do anything else
    /*
    this.progress.element = $('<div class="ajax-progress ajax-progress-fullscreen">&nbsp;</div>');
    $('body').after(this.progress.element);
    */
  };

  // There should no longer be a change in focus for success-calls.
  // (issue was seen when submitting multiple ajax-forms on the same page, the page-focus jumped.)
  Drupal.Ajax.prototype.success = function (response, status) {
    var _this = this;

    if (this.progress.element) {
      $(this.progress.element).remove();
    }
    if (this.progress.object) {
      this.progress.object.stopMonitoring();
    }

    // Remove the new class added through setProgressIndicatorThrobber.
    $(this.element).removeClass('js-loading');
    $(this.element).prop('disabled', false);

    var elementParents = $(this.element).parents('[data-drupal-selector]').addBack().toArray();

    var focusChanged = false;
    Object.keys(response || {}).forEach(function (i) {
      if (response[i].command && _this.commands[response[i].command]) {
        _this.commands[response[i].command](_this, response[i], status);
        if (response[i].command === 'invoke' && response[i].method === 'focus') {
          focusChanged = true;
        }
      }
    });

    /*
    if (!focusChanged && this.element && !$(this.element).data('disable-refocus')) {
      var target = false;

      for (var n = elementParents.length - 1; !target && n > 0; n--) {
        target = document.querySelector('[data-drupal-selector="' + elementParents[n].getAttribute('data-drupal-selector') + '"]');
      }

      if (target) {
        $(target).trigger('focus');
      }
    }
    */
    if (this.$form) {
      var settings = this.settings || drupalSettings;
      Drupal.attachBehaviors(this.$form.get(0), settings);
    }

    this.settings = null;
  };

})(jQuery);
